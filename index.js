'use strict';

const nconf   = require('nconf');
const AppServer = require('./lib/app/server');
const configs = require('./lib/infra/configs/config');
configs.initEnvironments(nconf);
const appServer = new AppServer();
const port = process.env.port || nconf.get('PORT') || 1337;
appServer.server.listen(port, 'localhost', async () => {
    console.log('%s started, listening at %s', appServer.server.name, appServer.server.url);
    //observer.init();
});