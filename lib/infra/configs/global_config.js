'use strict';

const nconf = require('nconf');

const getMSSQL = () => {
    return nconf.get('MSSQL_CONNECTION_STRING');
}

const getMongoProductDB = () => {
    return nconf.get('MONGO_LOCALHOST_PRODUCT_DATABASE');
}

const getMongoProductES = () => {
    return nconf.get('MONGO_LOCALHOST_PRODUCT_EVENT_STORE');
}

const getMongoProductView = () => {
    return nconf.get('MONGO_LOCALHOST_PRODUCT_VIEW');
}


const getMongoProductDOS = () => {
    return nconf.get('MONGO_LOCALHOST_PRODUCT_DOS');
}

const getMongoLegacyDOS = () => {
    return nconf.get('MONGO_LOCALHOST_LEGACY_DOS');
}

const getTopicARN = () => {
    return nconf.get('AWS_TOPIC_ARN_SQUID');
}

const getTopicQA = () => {
    return nconf.get('TOPIC_QA');
}

const getSQS = () => {
    const config = {
                    QueueUrl: nconf.get('AWS_SQS_TEST_SKU_SNS_ARN'),
                    MaxNumberOfMessages: 1, // how many messages do we wanna retrieve?
                    VisibilityTimeout: 30, // seconds - how long we want a lock on this job
                    WaitTimeSeconds: 20 // seconds - how long should we wait for a message?
    }
    return config;
}

const getTestSquidSQS = () => {
    const config = {
                    QueueUrl: nconf.get('AWS_SQS_TEST_SQUID_URL'),
                    MaxNumberOfMessages: 1, // how many messages do we wanna retrieve?
                    VisibilityTimeout: 30, // seconds - how long we want a lock on this job
                    WaitTimeSeconds: 20 // seconds - how long should we wait for a message?
    }
    return config;
}

const getAWSCredential = () => {
    return nconf.get('AWS_CREDENTIAL');
}

const getAZTSPolling = () => {
    return nconf.get('AZTS_CONNECTION_STRING');
}

const getUsernameAPI = () => {
    return nconf.get('NAKAMA_USER_API');
}

const getPasswordAPI = () => {
    return nconf.get('NAKAMA_PASSWORD_API');
}

module.exports = {
  getMSSQL: getMSSQL,
  getMongoProductDB: getMongoProductDB,
  getMongoProductES: getMongoProductES,
  getMongoProductView: getMongoProductView,
  getMongoProductDOS: getMongoProductDOS,
  getMongoLegacyDOS: getMongoLegacyDOS,
  getTopicARN: getTopicARN,
  getTopicQA: getTopicQA,
  getSQS: getSQS,
  getTestSquidSQS: getTestSquidSQS,
  getAWSCredential: getAWSCredential,
  getAZTSPolling: getAZTSPolling,
  getUsernameAPI: getUsernameAPI,
  getPasswordAPI: getPasswordAPI
}