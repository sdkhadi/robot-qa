'use strict';

const wrapper = require('../../../helpers/utils/wrapper');
const EndTesting = require('../../../helpers/tests/end_testing');

const getProduct = async () => {
    const api = 'http://nakama-core-api-1-nakama-core-api.apps.playcourt.id/api/products';
    const endTesting = new EndTesting(api,'auth');
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body.data[0].id,'isInteger',1);
            endTesting.expect(body.data[0].name,'isString',"Houseofcuff Square");
            endTesting.expect(body.data[0].image,'isString',"https://s3-ap-southeast-1.amazonaws.com/telkomdev/photo/fashion/1_HouseofcuffSquare_599000_fashion.jpg");
            endTesting.expect(body.data[0].category,'isString',"Fashion");
            endTesting.expect(body.data[1].id,'isInteger',2);
            endTesting.expect(body.data[1].name,'isString',"Jaket Sweater Harakiri");
            endTesting.expect(body.data[1].image,'isString',"https://s3-ap-southeast-1.amazonaws.com/telkomdev/photo/fashion/2_JaketSweaterHarakiri_159000_fashion.jpg");
            endTesting.expect(body.data[1].category,'isString',"Fashion");
            endTesting.expect(body.data[2].id,'isInteger',3);
            endTesting.expect(body.data[2].name,'isString',"Sepatu Varka V197");
            endTesting.expect(body.data[2].image,'isString',"https://s3-ap-southeast-1.amazonaws.com/telkomdev/photo/fashion/3_SepatuVarkaV197_120000_fashion.jpg");
            endTesting.expect(body.data[2].category,'isString',"Fashion");
        }
    }  
    scenarioTesting(body);  
}

const getDetailProduct = async () => {
    const api = 'http://nakama-core-api-1-nakama-core-api.apps.playcourt.id/api/detail-product/11';
    const endTesting = new EndTesting(api);
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body.success,'isString','true');
            // endTesting.expect(body.data.id,'isInteger',11);
            // endTesting.expect(body.data.name,'isString',"Sony E6833 Xperia Z5 Dual");
            // endTesting.expect(body.data.image,'isString',"https://s3-ap-southeast-1.amazonaws.com/telkomdev/photo/elektronik/11_SonyE6833XperiaZ5Dual_6850000_elektronik.jpg");
            // endTesting.expect(body.data.price,'isInteger',6850000);
            // endTesting.expect(body.data.stock,'isInteger',5);
            // endTesting.expect(body.data.description,'isString',"Sony Xperia Z5 Premium Dual E6883 Smartphone - Chrome [32 GB], hadir dengan layar touchscreen 5.50 Inch dengan resolusi 2160 piksel dengan 3840 pixel pada PPI dari 806 pixel per inci. Sony Xperia Z5 Premium Ganda didukung oleh octa-core Qualcomm Snapdragon 810 (MSM8994) prosesor dan dilengkapi dengan 3GB RAM. Telepon paket 32GB penyimpanan internal yang dapat diperluas hingga 200GB melalui kartu microSD. Sejauh kamera yang bersangkutan, Sony Xperia Z5 Premium Ganda bungkus kamera utama 23 megapiksel di bagian belakang dan depan penembak 5-megapiksel untuk narsis. Sony Xperia Z5 Premium Ganda menjalankan Android 5.1 dan didukung oleh baterai non removable 3430mAh. Ini ukuran 154,40 x 76,00 x 7,80 (tinggi x lebar x tebal) dan berat 180,00 gram. Sony Xperia Z5 Premium Dual dual SIM (GSM dan GSM) smartphone yang menerima dua Nano-SIM. Pilihan konektivitas termasuk Wi-Fi, GPS, Bluetooth, NFC, FM, 3G, 4G (dengan dukungan untuk Band 40 yang digunakan oleh beberapa jaringan LTE di India). Sensor di telepon termasuk sensor Proximity, sensor cahaya sekitar, Accelerometer, dan giroskop.");
            // endTesting.expect(body.data.category,'isString',"Elektronik");
            // endTesting.expect(body.message,'isString',"Get Product List");
            // endTesting.expect(body.code,'isInteger',200);
        }
    }  
    scenarioTesting(body);  
}


const getTestNakamart = async (req, res, next) => {
    getProduct();
    //getDetailProduct();
    res.send('End To End Testing for Team Profile Has Been Proceed');
}

module.exports = {
  getTestNakamart: getTestNakamart
}