'use strict';

const wrapper = require('../../../helpers/utils/wrapper');
const EndTesting = require('../../../helpers/tests/end_testing');
const users=require('../controllers/users')

const endPoint = 'https://api.indihome.co.id';
const getUsage = async () => {
    const api = endPoint +'/api/check-indihome-usage';
    
    console.log(api);
    const form = {
        'guid' :'9c09d323ccb25314e4fa287a706d7000' ,
        'data':'{ "nd":"141149102416"}'
    };
    const endTesting = new EndTesting(api,'auth',form,'form');
    //const getUser = new users(api,'u')
    const body = await endTesting.postIndihome();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body.info,'isString',"check_usage_success");
         
           //console.log();
        }
    }  
    scenarioTesting(body);  
}
const getMidContent = async () => {
    const api = endPoint +'/api/get-mid-content';
    console.log(api);
    const endTesting = new EndTesting(api,'auth');
    //const getUser = new users(api,'u')
    const body = await endTesting.getIndihome();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            //endTesting.expect(body.data[0].name,'isString',"Houseofcuff Square");
           endTesting.expect(body.info,'isString',"get_mid_promo");
        }
    }  
    scenarioTesting(body);  
}

const getTestIndihome = async (req, res, next) => {
    getUsage();
    getMidContent();
    res.send('End To End Testing for Team Profile Has Been Proceed');
}

module.exports = {
  getTestIndihome: getTestIndihome
}