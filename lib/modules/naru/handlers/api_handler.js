'use strict';

const wrapper = require('../../../helpers/utils/wrapper');
const EndTesting = require('../../../helpers/tests/end_testing');

const getNewsEvent = async () => {
    const api = 'http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/charities';
    const endTesting = new EndTesting(api);
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body[0]._id,'isString','5a29cb1f415607089cadf1dc');
            endTesting.expect(body[0].updatedAt,'isString',"2017-12-07T23:13:35.729Z");
            endTesting.expect(body[0].createdAt,'isString',"2017-12-07T23:13:35.729Z");
            endTesting.expect(body[0].newsTitle,'isStringString',"Gempa 5,3 SR guncang Bengkulu");
            endTesting.expect(body[0].newsDescription,'isString',"Gempa bumi berkekuatan 5,3 Skala Richter (SR) mengguncang daerah Kabupaten Kaur, Bengkulu pada Kamis malam.");
            endTesting.expect(body[0].newsImage,'isString',"https://img.antaranews.com/cache/730x487/2014/01/20140120gempa_bengkulu.jpg");
            endTesting.expect(body[0].newsAuthor,'isString',"antaranews.com");
            endTesting.expect(body[0].newsLocation,'isString',"Kaur, Bengkulu");
            endTesting.expect(body[0].newsCategory,'isString',"bencana");
            endTesting.expect(body[0].newsStartDate,'isString',"2017-12-08T00:39:00.123Z");
            endTesting.expect(body[0].newsEndDate,'isString',"2017-12-10T18:56:00.123Z");
            endTesting.expect(body[0].newsDisasterImpact,'isString',"");
            endTesting.expect(body[0].phoneNumber,'is',"");
            endTesting.expect(body[0].__v,'is',0);
        }
    }  
    scenarioTesting(body);  
}


const getPromos = async () => {
    const api = 'http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/promos';
    const endTesting = new EndTesting(api);
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body[0]._id,'isString','5a2d533691ecfb0e3cb8dbc3');
            endTesting.expect(body[0].newsPromoName,'isString',"Promo Natal & Tahun Baru Telkom");
            endTesting.expect(body[0].startDate,'isString',"2017-12-12T00:00:00.000Z");
            endTesting.expect(body[0].outDate,'isString',"2017-12-12T00:00:00.000Z");
            endTesting.expect(body[0].status,'isBoolean',true);
            endTesting.expect(body[0].newsCategory,'isString',"NON_BENCANA");
            endTesting.expect(body[0].newsPromoDescription,'isString',"Bulan Desember selalu dinanti oleh banyak orang. Ada yang bersiap-siap merayakan Natal, ada pula yang menyambut pergantian tahun dengan resolusi-resolusi baru. Hadiah kecil biasanya disiapkan untuk merayakan kedua momen tersebut. Terutama bagi keluarga dan sahabat.\r\nHadiah yang diberikan tidak melulu dalam bentuk barang. Bisa juga dalam bentuk paket promo menarik berikut ini. Yuk, pilih paket promo Natal & Tahun Baru favorit !");
            endTesting.expect(body[0].__v,'is',0);
            endTesting.expect(body[0].newsPromoImage[0],'isString',"https://s3-ap-southeast-1.amazonaws.com/temanberbagi/news/3b341b4b-008e-4abe-bb76-8b2273aedaae.jpg");
            endTesting.expect(body[0].newsPromoImage[1],'isString',"https://s3-ap-southeast-1.amazonaws.com/temanberbagi/news/0a56b043-717d-400d-8700-dca2e19851e9.png");
            endTesting.expect(body[0].newsPromoImage[2],'isString',"https://s3-ap-southeast-1.amazonaws.com/temanberbagi/news/aafbbeb6-b4b9-4e6c-a762-2d16ead939ee.jpg");
        }
    }  
    scenarioTesting(body);  
}


const getCamp = async () => {
    const api = 'http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/important-data/camp';
    const endTesting = new EndTesting(api);
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body[0]._id,'isString','5a3242c9d8ca401824fc083b');
            endTesting.expect(body[0].youtube_id,'isString',"5a2fefda96221315fc0bd8d2");
            endTesting.expect(body[0].location,'isString',"Pasar Seni Manggis, bali");
            endTesting.expect(body[0].important_data_category,'isString',"camp");
            endTesting.expect(body[1].__v,'is',0);
            endTesting.expect(body[1]._id,'isString','5a32b7a5881ae62a40336222');
            endTesting.expect(body[1].youtube_id,'isString',"5a2fefda96221315fc0bd8d2");
            endTesting.expect(body[1].location,'isString',"Desa Rendang, bali");
            endTesting.expect(body[1].important_data_category,'isString',"camp");
            endTesting.expect(body[1].__v,'is',0);
            endTesting.expect(body[2]._id,'isString','5a32b7b6881ae62a40336223');
            endTesting.expect(body[2].youtube_id,'isString',"5a2fefda96221315fc0bd8d2");
            endTesting.expect(body[2].location,'isString',"Desa Nongan, bali");
            endTesting.expect(body[2].important_data_category,'isString',"camp");
            endTesting.expect(body[2].__v,'is',0);
        }
    }  
    scenarioTesting(body);  
}


const getImportantData = async () => {
    const api = 'http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/important-data/';
    const endTesting = new EndTesting(api);
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body[0]._id,'isString','5a3241b8d8ca401824fc083a');
            endTesting.expect(body[0].youtube_id,'isString',"5a2fefda96221315fc0bd8d2");
            endTesting.expect(body[0].location,'isString',"JL. PASEKAN Gg.Batukaru Batubulan Gianyar Bali");
            endTesting.expect(body[0].important_data_category,'isString',"posko");
            endTesting.expect(body[1].__v,'is',0);
            endTesting.expect(body[1]._id,'isString','5a3242c9d8ca401824fc083b');
            endTesting.expect(body[1].youtube_id,'isString',"5a2fefda96221315fc0bd8d2");
            endTesting.expect(body[1].location,'isString',"Pasar Seni Manggis, bali");
            endTesting.expect(body[1].important_data_category,'isString',"camp");
            endTesting.expect(body[1].__v,'is',0);
            endTesting.expect(body[2]._id,'isString','5a32b78a881ae62a40336221');
            endTesting.expect(body[2].youtube_id,'isString',"5a2fefda96221315fc0bd8d2");
            endTesting.expect(body[2].location,'isString',"Pelabuhan Tanahampo, bali");
            endTesting.expect(body[2].important_data_category,'isString',"posko");
            endTesting.expect(body[2].__v,'is',0);
        }
    }  
    scenarioTesting(body);  
}


const getImportantDataPosko = async () => {
    const api = 'http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/important-data/posko';
    const endTesting = new EndTesting(api);
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body[0]._id,'isString','5a3241b8d8ca401824fc083a');
            endTesting.expect(body[0].youtube_id,'isString',"5a2fefda96221315fc0bd8d2");
            endTesting.expect(body[0].location,'isString',"JL. PASEKAN Gg.Batukaru Batubulan Gianyar Bali");
            endTesting.expect(body[0].important_data_category,'isString',"posko");
            endTesting.expect(body[0].__v,'is',0);
            endTesting.expect(body[1]._id,'isString','5a32b78a881ae62a40336221');
            endTesting.expect(body[1].youtube_id,'isString',"5a2fefda96221315fc0bd8d2");
            endTesting.expect(body[1].location,'isString',"Pelabuhan Tanahampo, bali");
            endTesting.expect(body[1].important_data_category,'isString',"posko");
            endTesting.expect(body[1].__v,'is',0);
        }
    }  
    scenarioTesting(body);  
}


const getYoutube = async () => {
    const api = 'http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/youtube';
    const endTesting = new EndTesting(api);
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body[0]._id,'isString','5a34b219e49d4b11380584c3');
            endTesting.expect(body[0].y_title,'isString',"CCTV Live Streaming Gunung Agung");
            endTesting.expect(body[0].y_category,'isString',"gunung meletus");
            endTesting.expect(body[0].y_desc,'isString',"CCTV Live Streaming Gunung Agung");
            endTesting.expect(body[0].y_status,'isBoolean',true);
            endTesting.expect(body[1].__v,'is',0);
            endTesting.expect(body[1].y_lokasi[0],'isString','Bali');
            endTesting.expect(body[1].y_lokasi[1],'isString','Jakarta');
            endTesting.expect(body[1].y_url[0],'isString',"https://www.youtube.com/embed/jQnh4Ly1Xpc");
            endTesting.expect(body[1].y_url[1],'isString',"https://www.youtube.com/embed/ddjw3NFoWBo");
            
        }
    }  
    scenarioTesting(body);  
}


const getCategory = async () => {
    const api = 'http://ec2-13-229-200-215.ap-southeast-1.compute.amazonaws.com:5000/youtube';
    const endTesting = new EndTesting(api);
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body[0].typecat,'isString','banjir');
            endTesting.expect(body[0].typecat,'isString','gempa');
            endTesting.expect(body[0].typecat,'isString','kebakaran');
            endTesting.expect(body[0].typecat,'isString','kecelakaan');
            endTesting.expect(body[0].typecat,'isString','longsor');
            endTesting.expect(body[0].typecat,'isString','upacara');
            endTesting.expect(body[0].typecat,'isString','acara');
            endTesting.expect(body[0].typecat,'isString','kunjungan');
            endTesting.expect(body[0].typecat,'isString','erupsi');
        }
    }  
    scenarioTesting(body);  
}


const getTestNaru = async (req, res, next) => {
    //getNewsEvent();
    getPromos();
    //getCamp();
    //getImportantData();
    //getImportantDataPosko();
    //getYoutube();
    //getCategory();
    res.send('End To End Testing for Naru Has Been Proceed');
}

module.exports = {
  getTestNaru: getTestNaru
}