'use strict';

const wrapper = require('../../../helpers/utils/wrapper');
const EndTesting = require('../../../helpers/tests/end_testing');

const getListBumn = async () => {
    const api = 'http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/list-bumn';
    const endTesting = new EndTesting(api,'auth');
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body.status,'isString','success');
            endTesting.expect(body.data[0]._id,'isString','5a372002b47a451a843f286a');
            endTesting.expect(body.data[0].bumnId,'isString',"000");
            endTesting.expect(body.data[0].bumnName,'isString',"Kementerian BUMN");
            endTesting.expect(body.data[0].categoryId,'isString',"14");
            endTesting.expect(body.data[0].imageUrl,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/logo/000.png");
            endTesting.expect(body.data[1]._id,'isString','5a372002b47a451a843f286d');
            endTesting.expect(body.data[1].bumnId,'isString',"010");
            endTesting.expect(body.data[1].bumnName,'isString',"PT ASABRI (Persero)");
            endTesting.expect(body.data[1].categoryId,'isString',"03");
            endTesting.expect(body.data[1].imageUrl,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/logo/010.png");
            endTesting.expect(body.data[2]._id,'isString','5a372003b47a451a843f28ae');
            endTesting.expect(body.data[2].bumnId,'isString',"515");
            endTesting.expect(body.data[2].bumnName,'isString',"PT Adhi Karya (Persero) Tbk");
            endTesting.expect(body.data[2].categoryId,'isString',"04");
            endTesting.expect(body.data[2].imageUrl,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/logo/515.png");
            endTesting.expect(body.data[3]._id,'isString','5a372003b47a451a843f28af');
            endTesting.expect(body.data[3].bumnId,'isString',"516");
            endTesting.expect(body.data[3].bumnName,'isString',"PT Amarta Karya (Persero)");
            endTesting.expect(body.data[3].categoryId,'isString',"04");
            endTesting.expect(body.data[3].imageUrl,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/logo/516.png");
            endTesting.expect(body.data[4]._id,'isString','5a372002b47a451a843f2867');
            endTesting.expect(body.data[4].bumnId,'isString',"004");
            endTesting.expect(body.data[4].bumnName,'isString',"PT Aneka Tambang (Persero) Tbk");
            endTesting.expect(body.data[4].categoryId,'isString',"11");
            endTesting.expect(body.data[4].imageUrl,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/logo/004.png");
        }
    }  
    scenarioTesting(body);  
}

// const postLogin = async () => {
//     const api = 'http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/login?officer=true';
//     const endTesting = new EndTesting(api,'auth');
//     const body = await endTesting.getResponseAPI();
//     const scenarioTesting = (result) => {
//         if(result.err){
//             console.log(result.err);
//         }else{
//             const body = result.data;
//             endTesting.expect(body.status,'isString','success');
//             endTesting.expect(body.data[0]._id,'isString','5a372002b47a451a843f286a');
//             endTesting.expect(body.data[0].bumnId,'isString',"000");
//             endTesting.expect(body.data[0].bumnName,'isString',"Kementerian BUMN");
//             endTesting.expect(body.data[0].categoryId,'isString',"14");
//             endTesting.expect(body.data[0].imageUrl,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/logo/000.png");
//         }
//     }  
//     scenarioTesting(body);  
// }

// const getLogin = async () => {
//     const api = 'http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/login?officer=true';
//     const endTesting = new EndTesting(api,'auth');
//     const body = await endTesting.getResponseAPI();
//     const scenarioTesting = (result) => {
//         if(result.err){
//             console.log(result.err);
//         }else{
//             const body = result.data;
//             endTesting.expect(body.status,'isString','success');
//             endTesting.expect(body.data[0]._id,'isString','5a372002b47a451a843f286a');
//             endTesting.expect(body.data[0].bumnId,'isString',"000");
//             endTesting.expect(body.data[0].bumnName,'isString',"Kementerian BUMN");
//             endTesting.expect(body.data[0].categoryId,'isString',"14");
//             endTesting.expect(body.data[0].imageUrl,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/logo/000.png");
//         }
//     }  
//     scenarioTesting(body);  
// }

const getCategoryBumn = async () => {
    const api = 'http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/category-bumn';
    const endTesting = new EndTesting(api,'auth');
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body.status,'isString','success');
            endTesting.expect(body.data[0]._id,'isString','5a3722636f283c1c5305fe4b');
            endTesting.expect(body.data[0].categoryName,'isString',"Telekomunikasi");
            endTesting.expect(body.data[0].category,'isString',"01");
            endTesting.expect(body.data[1]._id,'isString','5a3722636f283c1c5305fe4c');
            endTesting.expect(body.data[1].categoryName,'isString',"Transportasi");
            endTesting.expect(body.data[1].categoryId,'isString',"02");
            endTesting.expect(body.data[2]._id,'isString','5a3722646f283c1c5305fe4d');
            endTesting.expect(body.data[2].categoryName,'isString',"Jasa Keuangan");
            endTesting.expect(body.data[2].categoryId,'isString',"03");
            endTesting.expect(body.data[3]._id,'isString','5a3722646f283c1c5305fe4e');
            endTesting.expect(body.data[3].categoryName,'isString',"Konstruksi");
            endTesting.expect(body.data[3].categoryId,'isString',"04");
            endTesting.expect(body.data[4]._id,'isString','5a3722656f283c1c5305fe4f');
            endTesting.expect(body.data[4].categoryName,'isString',"Konsultan");
            endTesting.expect(body.data[4].categoryId,'isString',"05");
            endTesting.expect(body.code,'isInteger',"200");
            endTesting.expect(body.message,'isString',"Get list BUMN");
        }
    }  
    scenarioTesting(body);  
}

const getBumnbyBumnId = async () => {
    const api = 'http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/bumn/004';
    const endTesting = new EndTesting(api,'auth');
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body.status,'isString','success');
            endTesting.expect(body.data._id,'isString','5a372002b47a451a843f2867');
            endTesting.expect(body.data.bumnId,'isString',"004");
            endTesting.expect(body.data.bumnName,'isString',"PT Aneka Tambang (Persero) Tbk");
            endTesting.expect(body.data.categoryId,'isString',"11");
            endTesting.expect(body.data.imageUrl,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/logo/004.png");
            endTesting.expect(body.code,'isInteger',"200");
            endTesting.expect(body.message,'isString',"Get BUMN by ID");
        }
    }  
    scenarioTesting(body);  
}
    
const getBumnbyCategoryId = async () => {
    const api = 'http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/list-bumn/02';
    const endTesting = new EndTesting(api,'auth');
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body.status,'isString','success');
            endTesting.expect(body.data[0]._id,'isString','5a372002b47a451a843f2867');
            endTesting.expect(body.data[0].bumnId,'isString',"004");
            endTesting.expect(body.data[0].bumnName,'isString',"PT Aneka Tambang (Persero) Tbk");
            endTesting.expect(body.data[0].categoryId,'isString',"11");
            endTesting.expect(body.data[0].imageUrl,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/logo/004.png");
            endTesting.expect(body.data[1]._id,'isString','5a372002b47a451a843f286b');
            endTesting.expect(body.data[1].bumnId,'isString',"007");
            endTesting.expect(body.data[1].bumnName,'isString',"PT ASDP Indonesia Ferry (Persero)");
            endTesting.expect(body.data[1].categoryId,'isString',"02");
            endTesting.expect(body.data[1].imageUrl,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/logo/007.png");
            endTesting.expect(body.data[2]._id,'isString','5a372002b47a451a843f2868');
            endTesting.expect(body.data[2].bumnId,'isString',"005");
            endTesting.expect(body.data[2].bumnName,'isString',"PT Angkasa Pura II (Persero)");
            endTesting.expect(body.data[2].categoryId,'isString',"02");
            endTesting.expect(body.data[2].imageUrl,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/logo/005.png");
            endTesting.expect(body.code,'isInteger',"200");
            endTesting.expect(body.message,'isString',"Get list BUMN By Cat ID");
        }
    }  
    scenarioTesting(body);  
}

const getProfile = async () => {
    const api = 'http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/profile?page=1&count=10';
    const endTesting = new EndTesting(api,'auth');
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body.status,'isString','success');
            endTesting.expect(body.data.err,'isEmpty',null);
            endTesting.expect(body.data.data[0]._id,'isString','5a39b76c5fdb1b07784b71b0');
            endTesting.expect(body.data.data[0].nik,'isString',"0379194772");
            endTesting.expect(body.data.data[0].profilePicture,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/user/default_profile_picture.jpeg");
            endTesting.expect(body.data.data[0].name,'isString',"LACHRIMA PARAMITA RICHY");
            endTesting.expect(body.data.data[0].userId,'isString',"83724618-3c71-4581-a3b8-aa9b7505c7a4");
            endTesting.expect(body.data.data[0].mobilePhone,'isString',"082136398500");
            endTesting.expect(body.data.data[0].bumnName,'isString',"BANK MANDIRI");
            endTesting.expect(body.data.data[0].loginStatus,'isBoolean',false);
            endTesting.expect(body.data.data[0].email,'isString',"");
            endTesting.expect(body.data.data[0].isVerified,'isBoolean',false);
            endTesting.expect(body.data.data[0].bumnId,'isString',"008");
            endTesting.expect(body.data.data[1]._id,'isString','5a39b76c5fdb1b07784b71b1');
            endTesting.expect(body.data.data[1].nik,'isString',"0379194871");
            endTesting.expect(body.data.data[1].profilePicture,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/user/default_profile_picture.jpeg");
            endTesting.expect(body.data.data[1].name,'isString',"HAYUNING WIDIASARI");
            endTesting.expect(body.data.data[1].userId,'isString',"6ba449f5-52c3-4184-9fcb-578c0c1b644a");
            endTesting.expect(body.data.data[1].mobilePhone,'isString',"087851515346");
            endTesting.expect(body.data.data[1].bumnName,'isString',"BANK MANDIRI");
            endTesting.expect(body.data.data[1].loginStatus,'isBoolean',false);
            endTesting.expect(body.data.data[1].email,'isString',"");
            endTesting.expect(body.data.data[1].isVerified,'isBoolean',false);
            endTesting.expect(body.data.data[1].bumnId,'isString',"008");
            endTesting.expect(body.data.code,'isInteger',200);
            endTesting.expect(body.data.message,'isEMpty',null);
            endTesting.expect(body.meta.page,'isString',"1");
            endTesting.expect(body.meta.limit,'isString',"10");
            endTesting.expect(body.meta.totalPage,'isInteger',"20906");
            endTesting.expect(body.meta.totalRecord,'isInteger',"209052");
            endTesting.expect(body.code,'isInteger',200);
            endTesting.expect(body.message,'isString',"Get profile");
        }
    }  
    scenarioTesting(body);  
}

const getSummaryUser = async () => {
    const api = 'http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/summary-user';
    const endTesting = new EndTesting(api,'auth');
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body.status,'isString','success');
            endTesting.expect(body.data.userActiveCount,'isInteger',20617);
            endTesting.expect(body.data.controllerCount,'isInteger',288);
            endTesting.expect(body.data.pelaporCount,'isString',20617);
            endTesting.expect(body.data.allUserCount,'isString',209067);
            endTesting.expect(body.code,'isInteger',200);
            endTesting.expect(body.message,'isString',"Get user summary");
        }
    }  
    scenarioTesting(body);  
}

const getSummaryUserbyNIK = async () => {
    const api = 'http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/user?bumnId=111&nik=740059';
    const endTesting = new EndTesting(api,'auth');
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body.status,'isString','success');
            endTesting.expect(body.data.userId,'isInteger',"53bf37a9-ec39-4fcf-b5dc-db9d8108fede");
            endTesting.expect(body.data.profilePicture,'isInteger',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/user/1560232d-55cd-4101-ad65-824846a51d70.png");
            endTesting.expect(body.data.name,'isString',"ARI KURNIAWAN, ST");
            endTesting.expect(body.data.mobilePhone,'isString',"081219527576");
            endTesting.expect(body.data.email,'isString',"740059@telkom.co.id");
            endTesting.expect(body.data.nik,'isString',"740059");
            endTesting.expect(body.data.loginStatus,'isBoolean',false);
            endTesting.expect(body.data.bumnId,'isString',"111");
            endTesting.expect(body.data.bumnName,'isString',"PT Telekomunikasi Indonesia (Persero) Tbk");
            endTesting.expect(body.code,'isInteger',200);
            endTesting.expect(body.message,'isString',"Get user summary");
        }
    }  
    scenarioTesting(body);  
}

const getSummaryDetailPelapor = async () => {
    const api = 'http://ec2-13-229-74-222.ap-southeast-1.compute.amazonaws.com:9000/api/summary-detail-pelapor';
    const endTesting = new EndTesting(api,'auth');
    const body = await endTesting.getResponseAPI();
    const scenarioTesting = (result) => {
        if(result.err){
            console.log(result.err);
        }else{
            const body = result.data;
            endTesting.expect(body.status,'isString','success');
            endTesting.expect(body.data[0].bumnId,'isString',"111");
            endTesting.expect(body.data[0].count,'isInteger',11794);
            endTesting.expect(body.data[0].bumnName,'isString',"PT Telekomunikasi Indonesia (Persero) Tbk");
            endTesting.expect(body.data[0].images,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/logo/111.png");
            endTesting.expect(body.data[1].bumnId,'isString',"055");
            endTesting.expect(body.data[1].count,'isInteger',3079);
            endTesting.expect(body.data[1].bumnName,'isString',"PT Kereta Api Indonesia (Persero)");
            endTesting.expect(body.data[1].images,'isString',"https://s3-ap-southeast-1.amazonaws.com/sobatbumn/logo/055.png");
            endTesting.expect(body.data[2].bumnId,'isString',"");
            endTesting.expect(body.data[2].count,'isInteger',);
            endTesting.expect(body.data[2].bumnName,'isString',"");
            endTesting.expect(body.data[2].images,'isString',"");
            endTesting.expect(body.code,'isInteger',200);
            endTesting.expect(body.message,'isString',"Get user summary");
        }
    }  
    scenarioTesting(body);  
}


const getTestSobat = async (req, res, next) => {
    getListBumn();
    // postLogin();
    // getLogin();
    getCategoryBumn();
    getBumnbyBumnId();
    getBumnbyCategoryId();
    getProfile();
    // getSummaryUser(); *no respon
    // getSummaryUserbyNIK(); *no respon
    // getSummaryDetailPelapor();
    res.send('End To End Testing for API Sobat Has Been Proceed');
}

module.exports = {
  getTestSobat: getTestSobat
}