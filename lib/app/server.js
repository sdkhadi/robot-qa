'use strict';

const restify = require('restify');
const serveStatic = require('serve-static-restify');
const project = require('../../package.json');
const basicAuth = require('../auth/basic_auth_helper');
const naruHandler = require('../modules/naru/handlers/api_handler');
const sobatHandler = require('../modules/sobat/handlers/api_handler');
const nakamartHandler = require('../modules/nakamart/handlers/api_handler');
const indihometHandler = require('../modules/myIndihome/handlers/api_handler');
// const connectionHandler = require('../modules/connection/handlers/api_handler');
// const userHandler = require('../modules/user/handlers/api_handler');
const wrapper = require('../helpers/utils/wrapper');

let AppServer = function(){
  this.server = restify.createServer({
    name: project.name + '-server',
    version: project.version
  });

  this.server.serverKey = '';
  this.server.use(restify.acceptParser(this.server.acceptable));
  this.server.use(restify.queryParser());
  this.server.use(restify.bodyParser());
  this.server.use(restify.authorizationParser());
  this.server.use(basicAuth.init());

  //anonymous can access the end point, place code bellow
  this.server.get('/', (req, res) => {
    wrapper.response(res,'success',wrapper.data(`Quality Assurance Robot`),`This Service is Running Properly`);
  }); 

  
  //this.server.get('/api/testNaru', naruHandler.getTestNaru);
  //this.server.get('/api/testNakamart', nakamartHandler.getTestNakamart);
  //this.server.get('/api/testSobat', sobatHandler.getTestSobat);
  this.server.get('/api/myIndihome',indihometHandler.getTestIndihome)

   
  //authenticated user can access the end point, place code bellow
};

module.exports = AppServer;
