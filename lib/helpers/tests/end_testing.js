'use strict';

const Emitter = require('../events/event_emitter');
const config = require('../../infra/configs/global_config');
const request = require('request-promise');
const SNS = require('../components/aws-sns/sns');
const validate = require("validate.js");
const wrapper = require('../utils/wrapper');

class EndTesting{
    constructor(api=``,type=`noauth`,form=``){
        this.api = api;
        this.form = form;
        this.type = type;
        this.topicARN = config.getTopicQA();
    }

    async getResponseAPI(api=this.api,type=this.type){
        let option;
        let payload;
        switch (type){
        case 'auth':
            const auth = "Basic " + new Buffer(config.getUsernameAPI() + ":" + config.getPasswordAPI()).toString("base64");
            option = {url:api, headers : {"Authorization" : auth},timeout:10000};
            break;
        case 'noauth':
            option = {url:api,timeout:10000};
            break;
        }
        try{
            const body = await request.get(option);
            try{
                JSON.parse(body);
                return wrapper.data(JSON.parse(body));
            }catch(err){
                return wrapper.data(body);
            }
        }catch(err){
            payload = err.message;
            console.log(payload);
            this.sendEmail(payload);
            return wrapper.data(payload);
        }
    }
    async postResponseAPI(api=this.api,type=this.type){
        let option;
        let payload;
        switch (type){
        case 'auth':
            const auth = "Basic " + new Buffer(config.getUsernameAPI() + ":" + config.getPasswordAPI()).toString("base64");
            option = {
                url:api, 
                headers : {
                    "Authorization" : auth, 
                    "Content-Type" : "application/x-www-form-urlencoded"
                },
                form: {
                    'guid' :'c0e2c79918ab7dbd57ffd7647b8966a7' ,
                    'data':'{ "nd":"141149102416"}'
                },
                timeout:10000
            };
            break;
        case 'noauth':
            option = {url:api,timeout:10000};
            break;
        }
        try{
            const body = await request.post(option);
            console.log(body)
            try{
                JSON.parse(body);
                return wrapper.data(JSON.parse(body));
            }catch(err){
                return wrapper.data(body);
            }
        }catch(err){
            payload = err.message;
            console.log(payload);
            this.sendEmail(payload);
            return wrapper.data(payload);
        }
    }
    async getIndihome(api=this.api,type=this.type){
        let option;
        let payload;
        switch (type){
        case 'auth':
            const auth = "Basic bXlpbmRpaG9tZTpwN2Qya0xYNGI0TkY1OFZNODR2Vw";
            option = {url:api, headers : {"Authorization" : auth},timeout:10000};
            break;
        case 'noauth':
            option = {url:api,timeout:10000};
            break;
        }
        try{
            const body = await request.get(option);
            //console.log(body)
            try{
                JSON.parse(body);
                return wrapper.data(JSON.parse(body));
            }catch(err){
                return wrapper.data(body);
            }
        }catch(err){
            payload = err.message;
            console.log(payload);
            this.sendEmail(payload);
            return wrapper.data(payload);
        }
    }
    async postIndihome(api=this.api,type=this.type,form=this.form){
        let option;
        let payload;
        switch (type){
        case 'auth':
            const auth = "Basic bXlpbmRpaG9tZTpwN2Qya0xYNGI0TkY1OFZNODR2Vw";
            option = {
                url:api, 
                headers : {
                    "Authorization" : auth, 
                    "Content-Type" : "application/x-www-form-urlencoded"
                },
                form:form
                ,
                timeout:10000
            };
            break;
        case 'noauth':
            option = {url:api,timeout:10000};
            break;
        }
        try{
            const body = await request.post(option);
           console.log(body)
            try{
                JSON.parse(body);
                return wrapper.data(JSON.parse(body));
            }catch(err){
                return wrapper.data(body);
            }
        }catch(err){
            payload = err.message;
            console.log(payload);
            this.sendEmail(payload);
            return wrapper.data(payload);
        }
    }
    async expect(data,operator,value=null){
        let payload;
        switch (operator){
        case 'is':
            if(data===value){
                payload = `valid: ${data} is equal ${value} on ${this.api}`;
                console.log(payload);
            }else{
                payload = `error: ${data} is not equal with ${value} on ${this.api}`;
                console.log(payload);
                this.sendEmail(payload);
            }
            break;
        case 'isMax':
            if(data>=value){
                payload = `valid: value ${data} is >= ${value} on ${this.api}`;
                console.log(payload);
            }else{
                payload = `error: value ${data} is not >= ${value} on ${this.api}`;
                console.log(payload);
                this.sendEmail(payload);
            }
            break;
        case 'isJSON':
            try{
                JSON.parse(JSON.stringify(data));
                payload = `valid: ${JSON.stringify(data)} is JSON object on ${this.api}`;
                console.log(payload);
            }catch(err){
                payload = `error: ${JSON.stringify(data)} is not JSON object on ${this.api}`;
                console.log(payload);
                this.sendEmail(payload);
            }
            break;
        case 'isArray':
            if(validate.isArray(data)){
                payload = `valid: ${JSON.stringify(data)} is Array on ${this.api}`;
                console.log(payload);
            }else{
                payload = `error: ${JSON.stringify(data)} is not Array on ${this.api}`;
                console.log(payload);
                this.sendEmail(payload);
            }
            break;
        case 'isInteger':
            if(validate.isInteger(data)){
                payload = `valid: ${data} is Integer on ${this.api}`;
                console.log(payload);
            }else{
                payload = `error: ${data} is not Integer on ${this.api}`;
                console.log(payload);
                this.sendEmail(payload);
            }
            break;
        case 'isBoolean':
            if(validate.isBoolean(data)){
                payload = `valid: ${data} is Boolean on ${this.api}`;
                console.log(payload);
            }else{
                payload = `error: ${data} is not Boolean on ${this.api}`;
                console.log(payload);
                this.sendEmail(payload);
            }
            break;
        case 'isString':
            if(validate.isString(data)){
                payload = `valid: ${data} is String on ${this.api}`;
                console.log(payload);
            }else{
                payload = `error: ${data} is not String on ${this.api}`;
                console.log(payload);
                this.sendEmail(payload);
            }
            break;
        case 'isFloat':
            if(validate.isFloat(data)){
                payload = `valid: ${data} is Float on ${this.api}`;
                console.log(payload);
            }else{
                payload = `error: ${data} is not Float on ${this.api}`;
                console.log(payload);
                this.sendEmail(payload);
            }
            break;
        case 'isNotEmpty':
            if(!validate.isEmpty(data)){
                payload = `valid: ${data} is not Empty on ${this.api}`;
                console.log(payload);
            }else{
                payload = `error: ${data} is Empty on ${this.api}`;
                console.log(payload);
                this.sendEmail(payload);
            }
            break;
        case 'isNotNothing':
            if(validate.isNothing(data)){
                payload = `valid: ${data} is nothing on ${this.api}`;
                console.log(payload);
            }else{
                payload = `error: ${data} is exist on ${this.api}`;
                console.log(payload);
                this.sendEmail(payload);
            }
            break;
        case 'isNumber':
            if(validate.isNumber(data)){
                payload = `valid: ${data} is number on ${this.api}`;
                console.log(payload);
            }else{
                payload = `error: ${data} is not number on ${this.api}`;
                console.log(payload);
                this.sendEmail(payload);
            }
            break;
        case 'isDate':
            if(validate.isDate(data)){
                payload = `valid: ${data} is type of date on ${this.api}`;
                console.log(payload);
            }else{
                payload = `error: ${data} is not type of date on ${this.api}`;
                console.log(payload);
                this.sendEmail(payload);
            }
            break;
        }
    }

    async sendEmail(payload=`Error: ${this.api}`,topicARN=this.topicARN){
        const sns = new SNS(topicARN);
        sns.init();
        const result = await sns.publishTopic(payload,topicARN).then(data =>{
            console.log(`${this.api}, send error: ${payload}!!!`);
        }).catch(err =>{
            console.log(`${this.api} is error : ${payload} but failed to send email!!!`);
        });
    }
}

module.exports = EndTesting;