'use strict';

const cron = require('node-cron');
/*
 # ┌────────────── second (optional)
 # │ ┌──────────── minute
 # │ │ ┌────────── hour
 # │ │ │ ┌──────── day of month
 # │ │ │ │ ┌────── month
 # │ │ │ │ │ ┌──── day of week
 # │ │ │ │ │ │
 # │ │ │ │ │ │
 # * * * * * *
 #.start()
 #.stop()
 #.destroy()
 */

class CRON{
  constructor(schedule){
    this.schedule = schedule;
  }

  executeJob(job,schedule=this.schedule) {
    cron.schedule(schedule,job);
  } 

  createJob(job,schedule=this.schedule){
    let task = cron.schedule(schedule, job, false);
    return task;
  }
}

module.exports = CRON;