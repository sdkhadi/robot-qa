'use strict';

const MSSQL = require('../../databases/mssql/db');
const Mongo = require('../../databases/mongodb/db');
const SNS = require('../../components/aws-sns/sns');
const Emitter = require('../../events/event_emitter');
const EventPublisher = require('../../events/event_publisher');

class BATCH{
  constructor(configMSSQL,sqlQuery,configMongo,collection,database,topicARN,version=0,priority=1,status=1){
    this.configMSSQL = configMSSQL;
    this.sqlQuery = sqlQuery;
    this.configMongo = configMongo;
    this.collection = collection;
    this.database = database;
    this.topicARN = topicARN;
    this.version = version;
    this.priority = priority;
    this.status = status;
  }

  init(){
    const configMSSQL = this.configMSSQL;
    const configMongo = this.configMongo;
    const collection = this.collection;
    this.mssql = new MSSQL(configMSSQL);
    this.mongo = new Mongo(configMongo);
    this.mongo.setCollection(collection);
  }

  batchEventing(){
    this.batchReading();
  }

  async batchReading(){
    const db = this.mssql;
    const sqlQuery = this.sqlQuery;
    const recordset = await db.findMany(sqlQuery); 
    if(recordset.err){
      Emitter.emitEvent('error',recordset.err);  
    }else{
      recordset.data.map(record => {
        this.batchChecking(record);
      });
    }
  }

  async batchChecking(record){
    const db = this.mongo;
    const parameter = record;
    const recordset = await db.findOne(parameter);
    if(recordset.err) {
      (recordset.code!==404) ? Emitter.emitEvent('error',recordset.err) : this.batchWriting(record);     
    }else{
      Emitter.emitEvent('existed',JSON.stringify(record));
    }
  }

  getParameterWriting(record){
    const collection = this.collection;
    let parameter;
    switch (collection){
    case 'tlu_inveCPBrand':
      parameter = {vBrandID:record.vBrandID};
      break;
    case 'tlu_inveCPCategory':
      parameter = {vCatID:record.vCatID};
      break;
    case 'trx_inveComputer':
      parameter = {vPartID:record.vPartID};
      break;
    }
    return parameter;
  }

  async batchWriting(record) {
    const db = this.mongo;
    const document = record;
    const parameter = this.getParameterWriting(record);
    const updateQuery = {$set:document};
    const result = await db.upsertOne(parameter,updateQuery);
    if(result.err) {
      Emitter.emitEvent('error',result.err);     
    }else{
      const nModified = result.data.result.nModified;
      this.batchFindingObjectId(parameter,nModified);
    }
  }

  async batchFindingObjectId(parameter,nModified) {
    const db = this.mongo;
    const recordset = await db.findOne(parameter);
    (recordset.err) ? Emitter.emitEvent('error',recordset.err) : this.batchPublishing(recordset.data,nModified);
  }

  async batchPublishing(data,nModified) {
    let eventType;
    let eventDescription;
    switch (nModified){
    case 0:
        eventType = `${this.collection}Created`;
        eventDescription = `create ${this.collection}`;
        break;
    case 1:
        eventType = `${this.collection}Updated`;
        eventDescription = `update ${this.collection}`;
        break;
    }
    const eventPublisher = new EventPublisher(eventType,data,this.version,data._id,this.database,eventDescription);
    const result = await eventPublisher.publishEvent(this.topicARN);
    (result.err) ? Emitter.emitEvent('error',result.err) : console.log(result.data);
  }
}

module.exports = BATCH;