'use strict';

const Mongo = require('mongodb').MongoClient;
const Emitter = require('../../events/event_emitter');
const wrapper = require('../../utils/wrapper');
const validate = require("validate.js");

class DB{
    constructor(config){
        this.config = config;
    }

    setCollection(collectionName){
        this.collectionName = collectionName;
    }

    async findOne(parameter) {
        const config = this.config;
        const collectionName = this.collectionName;
        try{
            const connection = await Mongo.connect(config);
            const db = connection.collection(collectionName);
            const recordset = await db.findOne(parameter);
            connection.close();
            if(validate.isEmpty(recordset)){
                return await wrapper.error(`Data Not Found`,`Please Try Another Input`,404);
            }else{
                return await wrapper.data(recordset);
            }
        }catch(err){
            return await wrapper.error(err,err.message,503);
        };
    }

    async findMany(parameter) {
        const config = this.config;
        const collectionName = this.collectionName;
        try{
            const connection = await Mongo.connect(config);
            const db = connection.collection(collectionName);
            const recordset = await db.find(parameter).toArray();
            connection.close();
            if(validate.isEmpty(recordset)){
                return wrapper.error(`Data Not Found`,`Please Try Another Input`,404);
            }else{
                return wrapper.data(recordset);
            }
        }catch(err){
            return wrapper.error(err,err.message,503);
        };
    }

    async insertOne(document) {
        const config = this.config;
        const collectionName = this.collectionName;
        try{
            const connection = await Mongo.connect(config);
            const db = connection.collection(collectionName);
            const recordset = await db.insertOne(document);
            connection.close();
            if(recordset.result.n!==1) {
                return wrapper.error(`Internal Server Error`,`Failed Inserting Data to Database`,500);
            }else {
                return wrapper.data(document);
            }
        }catch(err){
            return wrapper.error(err,err.message,503);
        };
    }

    async insertMany(data) {
        const document = data;
        const config = this.config;
        const collectionName = this.collectionName;
        try{
            const connection = await Mongo.connect(config);
            const db = connection.collection(collectionName);
            const recordset = await db.insertMany(document);
            connection.close();
            if(recordset.result.n<1) {
                return wrapper.error(`Internal Server Error`,`Failed Inserting Data to Database`,500);
            }else {
                return wrapper.data(document);
            }
        }catch(err){
            return wrapper.error(err,err.message,503);
        };
    }

    async upsertOne(parameter,updateQuery) {
        const config = this.config;
        const collectionName = this.collectionName;
        try{
            const connection = await Mongo.connect(config);
            const db = connection.collection(collectionName);
            const recordset = await db.update(parameter,updateQuery,{ upsert: true });
            connection.close();
            return wrapper.data(recordset);
        }catch(err){
            return wrapper.error(err,err.message,503);
        };
    }
}

module.exports = DB;