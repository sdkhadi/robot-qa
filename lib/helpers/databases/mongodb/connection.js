'use strict';

const Mongo = require('mongodb').MongoClient;
const wrapper = require('../../utils/wrapper');
const validate = require("validate.js");
const Emitter = require('../../events/event_emitter');
const config = require('../../../infra/configs/global_config');

let connectionPool = [];
const connection = () => 
{
    const connectionState = {index:null,config: '',db: null};
    return connectionState;    
}
const init = () => {
    addConnectionPool();
    createConnectionPool();
}

const addConnectionPool = () => {
    // const connectionMongoCoreNav = connection();
    // const connectionMongoStockES = connection();
    // const connectionMongoStockView = connection();
    const connectionMongoProductView = connection();
    // connectionMongoCoreNav.index = 0;
    // connectionMongoStockES.index = 1;
    // connectionMongoStockView.index = 2;
    connectionMongoProductView.index = 2;
    // connectionMongoCoreNav.config = config.getMongoCoreNav();
    // connectionMongoStockES.config = config.getMongoStockES();
    // connectionMongoStockView.config = config.getMongoStockView();
    connectionMongoProductView.config = config.getMongoProductView();
    // connectionPool.push(connectionMongoCoreNav);
    // connectionPool.push(connectionMongoStockES);
    // connectionPool.push(connectionMongoStockView);
    connectionPool.push(connectionMongoProductView);
}

const createConnectionPool = async () => {
    connectionPool.map(async (currentConnection,index) => {
        const result = await createConnection(currentConnection.config);
        if(result.err){
            connectionPool[index].db = currentConnection;
        }else{
            connectionPool[index].db = result.data;
        }
    });
}

const createConnection = async (config) => {
    const options = {poolSize:100,socketTimeoutMS:15000,connectTimeoutMS:15000};
    try{
        const connection = await Mongo.connect(config,options);
        return wrapper.data(connection);
    }catch(err){
        return wrapper.error(err,err.message,503);
    }
}


const ifExistConnection = async (config) => {
    let state = {};
    connectionPool.map((currentConnection,index) => {
        if(currentConnection.config===config){
            state = currentConnection;
        }
    });
    if(validate.isEmpty(state)){
        return wrapper.error('Connection Not Found','Connection Must be Created Before',404);
    }else{
        return wrapper.data(state);
    }
}

const isConnected = async (state) => {
    const connection = state.db;
    if(!connection.serverConfig.isConnected()){
        return wrapper.error('Connection Not Found','Connection Must be Created Before',404,state);
    }else{
        return wrapper.data(state);
    }
}

const getConnection = async (config) => {
    const checkConnection = async () => {
        const result = await ifExistConnection(config);
        if(result.err){
            return result;
        }else{
            const connection = await isConnected(result.data);
            return connection;
        }
    }
    const result = await checkConnection();
    if(result.err){
        const state = await createConnection(config);
        if(state.err){
            return state;
        }else{
            connection.pool[parseInt(result.data.index)].db = state.data;
            return wrapper.data(connectionPool[parseInt(result.data.index)]);
        }
    }else{
        return result;
    }
}

module.exports = {
    init: init,
    getConnection: getConnection   
}
